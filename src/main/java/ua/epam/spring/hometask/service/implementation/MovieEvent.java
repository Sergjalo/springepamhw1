package ua.epam.spring.hometask.service.implementation;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.ApplicationContext;

import ua.epam.spring.hometask.dao.EventDao;
import ua.epam.spring.hometask.dao.UserDao;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.EventService;

public class MovieEvent implements EventService{
	private Set<Event> events;
	private EventDao daoEv;
	
	public MovieEvent() {
	}
	
	public MovieEvent(ApplicationContext ctx) {
		 daoEv= (EventDao) ctx.getBean("daoEventMock");
	}

	@Override
	public Event save(Event object) {
		events.add(object);
		daoEv.save(object);		
		return object;
	}

	@Override
	public void remove(Event object) {
		events.remove(object);
		daoEv.remove(object);		
	}

	@Override
	public Event getById(Long id) {
		return daoEv.getById(id);
	}

	@Override
	public Collection<Event> getAll() {
		return daoEv.getAll();
	}

	@Override
	public Event getByName(String name) {
		return daoEv.getByName(name);
	}

}
