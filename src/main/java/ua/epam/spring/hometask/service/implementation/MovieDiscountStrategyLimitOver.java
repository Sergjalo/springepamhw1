package ua.epam.spring.hometask.service.implementation;

import java.time.LocalDateTime;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.DiscountStrategy;

public class MovieDiscountStrategyLimitOver implements DiscountStrategy{

	@Override
	public byte estimateDiscount(User user, Event event, LocalDateTime airDateTime, long numberOfTickets) {
		return 0;
	}

}
