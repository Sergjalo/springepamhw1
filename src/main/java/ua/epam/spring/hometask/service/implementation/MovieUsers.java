package ua.epam.spring.hometask.service.implementation;

import java.util.Collection;
import java.util.Set;

import org.springframework.context.ApplicationContext;

import ua.epam.spring.hometask.dao.UserDao;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.UserService;

public class MovieUsers implements UserService{
	
	private Set<User> users;
	private UserDao daoObj;
	
	public MovieUsers() {
	}
	
	public MovieUsers(ApplicationContext ctx) {
		 daoObj= (UserDao) ctx.getBean("daoUserMock");
	}

	@Override
	public User save(User object) {
		users.add(object);
		daoObj.save(object);		
		return object;
	}

	@Override
	public void remove(User object) {
		users.remove(object);
		daoObj.remove(object);		
	}

	@Override
	public User getById(Long id) {
		return daoObj.getById(id);
	}

	@Override
	public Collection<User> getAll() {
		return daoObj.getAll();
	}

	@Override
	public User getUserByEmail(String email) {
		return daoObj.getUserByEmail(email);
	}

}
