package ua.epam.spring.hometask.dao;

import java.util.Collection;
import javax.annotation.Nullable;

import ua.epam.spring.hometask.domain.Auditorium;

public interface AuditoriumDao {
	public @Nullable Collection<Auditorium> getAll();
	public @Nullable Auditorium getByName(String name);
}
