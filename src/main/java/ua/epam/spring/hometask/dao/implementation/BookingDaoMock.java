package ua.epam.spring.hometask.dao.implementation;

import java.time.LocalDateTime;
import java.util.Set;

import ua.epam.spring.hometask.dao.BookingDao;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;

public class BookingDaoMock implements BookingDao{

	@Override
	public double getTicketsPrice(Event event, LocalDateTime dateTime, User user, Set<Long> seats) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void bookTickets(Set<Ticket> tickets) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Set<Ticket> getPurchasedTicketsForEvent(Event event, LocalDateTime dateTime) {
		// TODO Auto-generated method stub
		return null;
	}

}
