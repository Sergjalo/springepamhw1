package ua.epam.spring.hometask.dao;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import ua.epam.spring.hometask.domain.User;

public interface UserDao extends DomainDao<User>{
	public @Nullable User getUserByEmail(@Nonnull String email);
	
}
