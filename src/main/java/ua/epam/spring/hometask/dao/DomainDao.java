package ua.epam.spring.hometask.dao;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;

import ua.epam.spring.hometask.domain.*;

public interface DomainDao <T extends DomainObject>{
    public T save(@Nonnull T object);
    public void remove(@Nonnull T object);
    public T getById(@Nonnull Long id);
    public @Nonnull Collection<T> getAll();
}
