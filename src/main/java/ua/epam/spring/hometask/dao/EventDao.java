package ua.epam.spring.hometask.dao;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import ua.epam.spring.hometask.domain.Event;

public interface EventDao extends DomainDao<Event>{
    public @Nullable Event getByName(@Nonnull String name);
}
