package ua.epam.spring.hometask.dao;

import java.time.LocalDateTime;
import java.util.Set;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;

public interface BookingDao {
	public double getTicketsPrice(Event event, LocalDateTime dateTime, User user, Set<Long> seats);
	public void bookTickets(Set<Ticket> tickets) ;
	public Set<Ticket> getPurchasedTicketsForEvent(Event event, LocalDateTime dateTime) ;
}
