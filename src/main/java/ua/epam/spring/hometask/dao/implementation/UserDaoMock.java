package ua.epam.spring.hometask.dao.implementation;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ua.epam.spring.hometask.dao.DomainDao;
import ua.epam.spring.hometask.dao.UserDao;
import ua.epam.spring.hometask.domain.*;

public class UserDaoMock implements UserDao{
	
	private static Set<User> h = new HashSet<User>(Arrays.asList(
			new User() {
				 private Long id=1L;
				 private String lastName="Pyatochkin";
				 private String firstName="Petya";
			 },
			new User() {
				 private Long id=2L;
				 private String lastName="Pyatochkin";
				 private String firstName="Vova";
			 },
			new User() {
				 private Long id=3L;
				 private String lastName="Saahov";
				 private String firstName="Givi";
			 }
			));

	@Override
	public User save(User object) {
		h.add(object);
		return object;
	}

	@Override
	public void remove(User object) {
		User u= this.getById(object.getId());
		h.remove(u);
	}

	@Override
	public User getById(Long id) {
      for (User ur : h) {
        if (ur.getId()==id) 
          return ur;
      }
      return null; 
	}

	@Override
	public Collection<User> getAll() {
		return h;
	}

	@Override
	public User getUserByEmail(String email) {
       for (User ur : h) {
	          if (ur.getEmail().equals(email)) 
	            return ur;
        }
        return null; 
	}


}
