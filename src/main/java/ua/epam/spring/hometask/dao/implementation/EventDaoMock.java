package ua.epam.spring.hometask.dao.implementation;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

import ua.epam.spring.hometask.dao.EventDao;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.EventService;

public class EventDaoMock implements EventDao{
	
	// ------------  fill with some test values
	private static Set<Event> h = new HashSet<Event>(Arrays.asList(
			new Event(){
				private String name="scary movie";
				private EventRating rating=EventRating.HIGH;
			 },
			new Event(){
					private String name="scary movie 2";
					private EventRating rating=EventRating.MID;
				 },
			new Event(){
						private String name="mirror";
						private EventRating rating=EventRating.LOW;
					 }
			));

	static {
		// --- all that is just to generate some different dates
		LocalDateTime now = LocalDateTime.now();
		NavigableSet<LocalDateTime> airDates0;
		NavigableSet<LocalDateTime> airDates = new TreeSet<>();
		airDates.add(now);
		airDates.add(now.plusDays(1)); 
		airDates.add(now.plusDays(-21)); 
		
		Iterator i = h.iterator();
		while (i.hasNext())
		  {
		    Event e = (Event) i.next();
		    airDates0 = new TreeSet<>();
		    airDates0.addAll(airDates);
		    airDates0.remove(airDates.first());
		    airDates0.add(airDates.first().plusDays(7));
		    e.setAirDates(airDates0);
		    e.assignAuditorium(airDates0.first(), new Auditorium());
		    e.assignAuditorium(airDates0.last(), new Auditorium());
		    airDates=airDates0;
		  }
	}
	// ------------  fill with some test values

	@Override
	public Event save(Event object) {
		h.add(object);
		return object;
	}

	@Override
	public void remove(Event object) {
		Event u= this.getById(object.getId());
		h.remove(u);
	}

	@Override
	public Event getById(Long id) {
	      for (Event ur : h) {
	          if (ur.getId()==id) 
	            return ur;
	        }
	        return null; 
	}

	@Override
	public Collection<Event> getAll() {
		return h;
	}

	@Override
	public Event getByName(String name) {
	       for (Event ur : h) {
		          if (ur.getName().equals(name)) 
		            return ur;
	        }
	        return null; 
	}

}
