package ua.epam.spring.hometask.domain;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ua.epam.spring.hometask.service.EventService;
import ua.epam.spring.hometask.service.UserService;
import ua.epam.spring.hometask.service.implementation.MovieUsers;
import ua.epam.spring.hometask.domain.ContextLauncher; 

/***
 * depricated since tests are used
 * @author Sergii_Kotov
 *
 */
public class Console {
	static ContextLauncher l;
	public static void main(String[] args) {
		UserService mp= (UserService)l.ctx.getBean("MUsers");
		System.out.println(mp.getAll().size());
		
		EventService es= (EventService)l.ctx.getBean("MEvents");
		es.getAll().forEach(a-> System.out.println(a));
		
	}

}
