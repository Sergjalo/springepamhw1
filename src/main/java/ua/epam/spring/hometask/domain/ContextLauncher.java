package ua.epam.spring.hometask.domain;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/***
 * Ну не знаю как передать контекст в другой бин при инициализации. поэтому завел отдельную 
 * "глобальную переменную" с контекстом
 * и ее использую в врыажении для инициализации бина в spring.xml
 * @author Sergii_Kotov
 *
 */
public class ContextLauncher {
	
	public static ApplicationContext ctx=new ClassPathXmlApplicationContext("classpath:src/main/resources/spring.xml");
}
