package ua.epam.spring.hometask.domain;

import static org.junit.Assert.*;

import org.junit.Test;

import ua.epam.spring.hometask.dao.UserDao;
import ua.epam.spring.hometask.service.UserService;
import ua.epam.spring.hometask.service.implementation.MovieUsers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:src/main/resources/spring.xml"})
public class TestUsers {
	/*
	 * не понадобился тестовый контекст.
	@Autowired
	ApplicationContext ctx;
	*/
	static ContextLauncher l;
	
	@Test
	public void testUserCount() {
		UserService mp=(UserService) l.ctx.getBean("MUsers");
		assertEquals(3, mp.getAll().size());
	}
	
}
